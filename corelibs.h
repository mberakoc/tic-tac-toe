#if !defined(CORELIBS)
#define CORELIBS

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef enum
{
    FALSE, TRUE
} Boolean;

typedef struct LocationHolder LocationHolder;
typedef LocationHolder * LocationHolderPtr;

struct LocationHolder
{
    unsigned location[2];
    LocationHolderPtr next_ptr;
    unsigned size;
};

int * intdup(int const * src, size_t len)
{
    int *dest = malloc(len * sizeof(int));
    memcpy(dest, src, len * sizeof(int));
    return dest;
}

void insert_location(LocationHolderPtr *location_holder_header_ptr, unsigned location[])
{
    LocationHolderPtr new_location_holder_ptr = (LocationHolderPtr) malloc(sizeof(LocationHolder));
    if (new_location_holder_ptr != NULL)
    {
        memcpy(new_location_holder_ptr->location, location, 2 * sizeof(int));
        new_location_holder_ptr->next_ptr = NULL;
        new_location_holder_ptr->size = 1;
        if (*location_holder_header_ptr == NULL)
        {
            *location_holder_header_ptr = new_location_holder_ptr;
            return;
        }
        LocationHolderPtr current_location_holder_ptr = *location_holder_header_ptr;
        while (current_location_holder_ptr->next_ptr != NULL)
        {
            ++current_location_holder_ptr->size;
            current_location_holder_ptr = current_location_holder_ptr->next_ptr;
        }
        new_location_holder_ptr->size = ++current_location_holder_ptr->size;
        current_location_holder_ptr->next_ptr = new_location_holder_ptr;
    }
    else
    {
        insert_location(location_holder_header_ptr, location);
    }
}

void delete_location_holder(LocationHolderPtr *location_holder_header_ptr)
{
    LocationHolderPtr current_ptr = *location_holder_header_ptr, next_ptr;
    while (current_ptr != NULL)
    {
        next_ptr = current_ptr->next_ptr;
        free(current_ptr);
        current_ptr = next_ptr;
    }
    *location_holder_header_ptr = NULL;
}

#endif // CORELIBS
