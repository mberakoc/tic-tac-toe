#if !defined(PLAYER)
#define PLAYER

#include "corelibs.h"
#include "board.h"

typedef struct Player Player;
typedef Player * PlayerPtr;

struct Player
{
    char symbol;
    Boolean is_winner;
    PlayerPtr id;
};

int turn_counter;

Player _Player(char symbol)
{
    PlayerPtr new_player_ptr = (PlayerPtr) malloc(sizeof(Player));
    if (new_player_ptr != NULL)
    {
        new_player_ptr->symbol = symbol;
        new_player_ptr->is_winner = FALSE;
        new_player_ptr->id = new_player_ptr;
        return *new_player_ptr;
    }
    else
    {
        _Player(symbol);
    }
    
}
int * choose_location(Board board);
void make_move(Player player, Board board)
{
    if ((board.id)->is_full) return;
    turn_counter++;
    int *square_index = choose_location(board);
    (board.id)->squares[square_index[0]][square_index[1]] = player.symbol;
    if (turn_counter == BOARD_SIZE * BOARD_SIZE)
    {
        (board.id)->is_full = TRUE;
        return;
    }
}

int * choose_location(Board board)
{
    LocationHolderPtr location_holder_header = NULL;
    for (int i = 0; i < BOARD_SIZE; ++i)
    {
        for (int j = 0; j < BOARD_SIZE; ++j)
        {
            if ((board.id)->squares[i][j] == ' ') insert_location(&location_holder_header, (int []){i, j});
        }
    }
    unsigned random_index = rand() % location_holder_header->size;
    for (int i = 0; i < random_index; ++i)
    {
        location_holder_header = location_holder_header->next_ptr;
    }
    int *location = intdup(location_holder_header->location, 2 * sizeof(int));
    delete_location_holder(&location_holder_header);
    return location;
}

void print_player(Player player)
{
    printf("{PlayerId: [@%p], Symbol: [%c], Win: [%s]}\n", &player, player.symbol, player.is_winner ? "True" : "False");
}

#endif // PLAYER
