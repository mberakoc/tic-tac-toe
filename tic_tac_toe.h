#if !defined(TIC_TAC_TOE)
#define TIC_TAC_TOE

#include <time.h>
#include <unistd.h>
#include "board.h"
#include "player.h"
#define WIN_CASES_NUMBER 8

typedef struct Game Game;
typedef Game * GamePtr;

struct Game
{
    Board board;
    Player players[2];
    GamePtr id;
};

const int WIN_CASES[8][3][2] = {{{0, 0}, {0, 1}, {0, 2}}, {{1, 0}, {1, 1}, {1, 2}}, {{2, 0}, {2, 1},
 {2, 2}}, {{0, 0}, {1, 0}, {2, 0}}, {{0, 1}, {1, 1}, {2, 1}}, {{0, 2}, {1, 2}, {2, 2}}, {{0, 0}, {1, 1}, {2, 2}}, {{0, 2}, {1, 1}, {2, 0}}};
Boolean is_game_finished;

Game _Game()
{
    GamePtr new_game_ptr = (GamePtr) malloc(sizeof(Game));
    if (new_game_ptr != NULL)
    {
        new_game_ptr->board = _Board();
        Player players[] = {_Player('X'), _Player('O')};
        memcpy(new_game_ptr->players, players, sizeof(players));
        new_game_ptr->id = new_game_ptr;
        return *new_game_ptr;
    }
    else
    {
        return _Game();
    }   
}

void check_current_state(Game game);
void choose_winner(Game game);
void init_game()
{
    system("clear");
    puts("*******[TIC TAC TOE]*******");
    srand(time(NULL));
    Game game = _Game();
    while (!(game.board.id)->is_full && !is_game_finished)
    {
        make_move(game.players[turn_counter % 2], game.board);
        check_current_state(game);
        sleep(1);
        system("clear");
        puts("*******[TIC TAC TOE]*******");
    }
    print_board(*(game.board.id));
    printf("Total Turns: %d\n", turn_counter);
    choose_winner(game);
    puts("Game finished.");
}

void choose_winner(Game game)
{
    Player first_player = *(game.players[0].id);
    Player second_player = *(game.players[1].id);
    if (first_player.is_winner)
    {
        printf("%c won!\n", first_player.symbol);
    } 
    else if (second_player.is_winner)
    {
        printf("%c won!\n", second_player.symbol);
    }
    else
    {
        puts("Draw!");
    }
}

Boolean are_three_number_equal(int first_number, int second_number, int third_number)
{
    if (first_number == second_number && second_number == third_number && first_number == third_number) return TRUE;
    return FALSE;
}

void check_current_state(Game game)
{
    Board board = *((game.id->board).id);
    print_board(board);
    Player players[2] = {game.id->players[0], game.id->players[1]};
    memcpy(players, game.id->players, 2);
    for (int i = 0; i < WIN_CASES_NUMBER; ++i)
    {
        int current_win_case[BOARD_SIZE][2];
        memcpy(current_win_case, WIN_CASES[i], sizeof(int) * BOARD_SIZE * 2);
        if (are_three_number_equal(board.squares[current_win_case[0][0]][current_win_case[0][1]], board.squares[current_win_case[1][0]][current_win_case[1][1]],
            board.squares[current_win_case[2][0]][current_win_case[2][1]]) && board.squares[current_win_case[0][0]][current_win_case[0][1]] != ' ')
        {
            if (board.squares[current_win_case[0][0]][current_win_case[0][1]] == players[0].symbol) ((game.id)->players[0].id)->is_winner= TRUE;
            else if (board.squares[current_win_case[0][0]][current_win_case[0][1]] == players[1].symbol) ((game.id)->players[1].id)->is_winner = TRUE;
            is_game_finished = TRUE;
            return;
        }
    }
}

#endif // TIC_TAC_TOE
